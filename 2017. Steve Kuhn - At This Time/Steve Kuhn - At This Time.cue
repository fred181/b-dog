REM GENRE Jazz
REM DATE 2017
REM DISCID 860DCA09
REM COMMENT "ExactAudioCopy v1.3"
PERFORMER "Steve Kuhn"
TITLE "At This Time"
FILE "01 - My Shining Hour.wav" WAVE
  TRACK 01 AUDIO
    TITLE "My Shining Hour"
    PERFORMER "Steve Kuhn"
    INDEX 01 00:00:00
  TRACK 02 AUDIO
    TITLE "Ah Moore"
    PERFORMER "Steve Kuhn"
    INDEX 00 06:28:65
FILE "02 - Ah Moore.wav" WAVE
    INDEX 01 00:00:00
  TRACK 03 AUDIO
    TITLE "The Pawnbroker"
    PERFORMER "Steve Kuhn"
    INDEX 00 09:15:15
FILE "03 - The Pawnbroker.wav" WAVE
    INDEX 01 00:00:00
  TRACK 04 AUDIO
    TITLE "All the Rest is the Same"
    PERFORMER "Steve Kuhn"
    INDEX 00 06:06:11
FILE "04 - All the Rest is the Same.wav" WAVE
    INDEX 01 00:00:00
  TRACK 05 AUDIO
    TITLE "The Feeling Within"
    PERFORMER "Steve Kuhn"
    INDEX 00 07:29:59
FILE "05 - The Feeling Within.wav" WAVE
    INDEX 01 00:00:00
  TRACK 06 AUDIO
    TITLE "Carousel"
    PERFORMER "Steve Kuhn"
    INDEX 00 05:44:11
FILE "06 - Carousel.wav" WAVE
    INDEX 01 00:00:00
  TRACK 07 AUDIO
    TITLE "Lonely Town"
    PERFORMER "Steve Kuhn"
    INDEX 00 05:47:19
FILE "07 - Lonely Town.wav" WAVE
    INDEX 01 00:00:00
  TRACK 08 AUDIO
    TITLE "This is New"
    PERFORMER "Steve Kuhn"
    INDEX 00 05:42:57
FILE "08 - This is New.wav" WAVE
    INDEX 01 00:00:00
  TRACK 09 AUDIO
    TITLE "I Waited for You"
    PERFORMER "Steve Kuhn"
    INDEX 00 05:40:27
FILE "09 - I Waited for You.wav" WAVE
    INDEX 01 00:00:00
                                                                                                                                                                                                                                                                                                                      